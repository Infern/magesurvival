Unity Version:
2020.3.15f2

Packages Used:
Input System 1.0.2
Cinemachine 2.6.11


Credits:

Wizard Model: https://assetstore.unity.com/packages/3d/characters/humanoids/fantasy/battle-wizard-poly-art-128097
Visual Effects: https://assetstore.unity.com/packages/vfx/particles/action-rpg-fx-38222
Sound Effects: https://assetstore.unity.com/packages/audio/sound-fx/ultimate-sound-fx-bundle-151756